const User = require('../models/User');
const bcrypt = require('bcrypt');
const auth = require('../auth');
const Course = require('../models/Course');

//check if the email already exists
/**
 * Steps:
 * 1. use mongoose "find" method to find duplicate emails
 * 2. use the "then" method to send a response back to the client based on the result of the find method (email already exists/not existing)
 */

module.exports.checkEmailExists = (reqBody) => {
    return User.find( {email: reqBody.email } ).then((result) => {
        //The "find" method returns a record if a match is found
        if(result.length > 0){
            return true;
        }else{
            //No duplicate email found
            //the user is not yet registered in the database
            return false;
        }
    })
}

//user registration
/**
 * 
 */
//uses the information from the request body to provide all necessary information
module.exports.registerUser = (reqBody) => {
    let newUser = new User({
        firstName : reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        mobileNo: reqBody.mobileNo,
        //10 is the value provided as the number of "salt" rounds that the bcrypt algorithm will run in order to encrypt the password
        password: bcrypt.hashSync(reqBody.password, 10)
    })

    //saves the created object to our database
    return newUser.save().then((user, error) => {
        //user registration failed
        if(error){
            return false;
        }else{
            //user registration is successful
            return true;
        }
    })
}
 

//User authentication
/**
 * steps:
 * 1. check the database if the user email exists
 * 2. compare the password provided in the login form with the password stored in the database
 * 3. Generate/return a JSON web token if the user is successfuly logged in and return false if not
 */

module.exports.loginUser = (reqBody) => {
    return User.findOne( {email: reqBody.email} ).then(result => {
        //if user does not exist
        if(result == null){
            return false;
        }else{
            //user exists
            //create a variable "isPasswordCorrect" to return the result of comparing the login form password and the database password
            //"compareSync" method is used to compare a non encrypted password from the login form to the encrypted password retrieved from the database and returns "true" or "false" value depending on the result
            //A good practice for boolean variable/constants is to use the word "is" or "are" at the beginning in the form of is+Noun
            //example: isAdmin, isDone, areDone
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
            //if the passwords match/result of the above code is true
            if(isPasswordCorrect){
                //generate an access token
                //use the "createAccessToken" method defined in the 'auth.js' file
                //returning an object back to the frontend
                //we will use the mongoose method "toObject" = it converts the mongoose object into a plain javascript object
                return { accessToken: auth.createAccessToken(result.toObject()) }
            }else{
                //password do not match
                return false;
            }
        }
    })
}

/**ACTIVITY option 1
 * module.exports.getProfile = (reqBody) => {
    
    return User.findOne({ _id: reqBody.id }).then(result => {
        //option 2 ({ reqBody.userId }) kita kay user routes
        if(result == null){
            return false;
        }else{
            result.password = "";
            // result.password = undefined; pwede rin to


            return result;
            
        }
       
    })
}
 */



module.exports.getProfile = (data) => {
	return User.findById( data.userId ).then(result => {

		result.password = "";

		return result;
	})
}


//enroll a user to a class/course
/***
 * Steps:
 * 1. Find the document in the database using the user's ID
 * 2. Add the course ID to the user's enrollment array, 
 * 3. save the data in the database
 * 4.  find the document in the database using the course's ID
 * 5. add the user id to the course's enrollees array
 * 4. save the data in the database
 * 7. error handling: if successful, return true,else return false
*/
 
//Async await will be used in the enrolling the user because we will need to update 2 separate documents when enrolling a user
module.exports.enroll = async (data) => {
    let isUserUpdated = await User.findById(data.userId).then(user => {
        //Add the courseID in the user's enrollments array
        user.enrollments.push({courseId: data.courseId})

            //saves the updated user information in the database
        return user.save().then((user, error) => {
            if(error){
                return false;

            }else{
                return true;
            }
        })
    })

    let isCourseUpdated = await Course.findById(data.courseId).then(course => {
        course.enrollees.push({userId: data.userId});

        return course.save().then((course, error) => {
            if(error){
                return false;
            }else{
                return true;
            }
        })
    })

    if(isUserUpdated && isCourseUpdated){
        return true;
    }else{
        return false;
    }
    
}