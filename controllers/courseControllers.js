const Course = require('../models/Course');

//Creation of Course


module.exports.addCourse = (data) => {
    if(data.isAdmin){
        let newCourse = new Course({
            name: data.course.name,
            description: data.course.description,
            price: data.course.price
        })

        return newCourse.save().then((course, error) => {
            //Course creation failed
            if(error){
                return false;
            }else{
                return true;
            }

        })
    }else{
        return false;
    }
}

//retrieve all courses
module.exports.getAllCourses = () => {
    return Course.find({}).then(result => {
        return result;
    })  
}

//retrieve all active courses
module.exports.getAllActive = () => {
    return Course.find({isActive: true}).then(result => {
        return result;
    }) 
}

//specific course
// module.exports.specificCourse = (reqParams) =>{
// 	return Course.findById(reqParams.courseId).then(result => {
// 		return result;
// 	})
// }

module.exports.specificCourse = (courseId) =>{
	return Course.findById(courseId)
}

//update course
//Activity: SIr PAulo code: https://gitlab.com/fernandezpaulo0724-zuitt-projects/b127/s35-a
module.exports.updateCourse = (reqParams, reqBody) => {

    let updatedCourse = {
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    }

    //findByIdAndUpdate(ID, updatesTObeApplied)
    return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
        if(error){
            return false;
        }else{
            return true;
        }
    })

            

}

/**
 * Steps:
 * 1. Check if admin(routes)
 * 2. create a variable where the is active will change into false
 * 3. so we can use findByIdAndUpdate(id, updatedVariable). then error handling, if course is not arhive, return false, if the course is archive successfully, return true
 */

//Archived a course
module.exports.archiveCourse = (reqParams) => {

    let updatedCourse = {
		isActive: false
	}

    return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, err) => {
        if(err){
            return false;
        }else{

            return true;
        }
    })
    
}

