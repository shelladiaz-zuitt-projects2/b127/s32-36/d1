const jwt = require('jsonwebtoken');
//user defined string data will be used to create our JSON web tokens
//used in the algorithm for encrypting our data which makes it difficult to decode the information without the defined secret keyword
const secret = 'CourseBookingAPI';

//JSON Web Token or JWT is a way of securely passing information from the server to the frontend or to the other part pf server

//TOKEN creation
//Analogy = Pack the gift and provide a lock with the secret code as the key
module.exports.createAccessToken = (user) => {
    
    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin
    };



    return jwt.sign(data, secret, {})


}

//token Verifiaction
//Analogy: REceive the gist and open the lock to verify if the sender is legitimate and the gift was not tampered with

module.exports.verify = (req, res, next) => {
    //The token is retrieved from  request header
    //This can be provided in postman under
        //Authorization > BEarer Token

    let token = req.headers.authorization;

    //Token received and is not undefined
    ///typeof use para macheck if ano type ba
    if(typeof token !== "undefined"){
        console.log(token);
        //The token that we receive is just like this:
            //"Beare eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9
        //The "slice" method takes only the token from information sent via request header
        //this removes the "Bearer " prefix and obtains onlt the token for verfivation

        token = token.slice(7, token.length);

        //validate the token using the "verify" method decrypting the token using the secret code
        return jwt.verify(token, secret, (err, data) => {
            if(err){
                return res.send( {auth: "failed"} )
            }else{
                //if JWT is valid
                //Allows the application to proceed with the next middleware function/callback function in the route
                //the next() middleware will ne used to proceed to another function that invokes the controller function(business logic)
                next()
            }
        })

    }else{
        return res.send({auth: "failed"});
    }
} 

//Token Decryption
//Analogy = open the gift and get the content
module.exports.decode = (token) => {

    //Token received and is not undefined
    if(typeof token !== "undefined"){
            //retrieves only the token and removes the "Bearer" prefix
        token = token.slice(7, token.length);

        return jwt.verify(token, secret, (err, data) => {
            if(err){
                return null;
            }else{
                //the "decode" method is used to obtain the info from the JWT
                // {complete:true} option allows us to return additional info from the JWT token
                //REturns an object with access to the "payload" property whicj contains information stored when the token was generated
                //the payload contains the info provided in the "createAccessToken" method defined above (id, email and isAdmin)
                return jwt.decode(token, {complete:true}).payload
            }
        })
        //token does not exist
    }else{
        return null;
    }

    
}