const express = require('express');
const mongoose = require('mongoose');
//allows our backend application to be available to our frontend application
//allows us  to control app's cross origin resources sharing settings
const cors = require('cors');

//allow access to routes defined within our application(main entry point)
const userRoutes = require('./routes/userRoutes')
const courseRoutes = require('./routes/courseRoute')

const app = express();

//Connect to our MongoDB database
mongoose.connect('mongodb+srv://admin:admin@zuitt-bootcamp.dxzdo.mongodb.net/Batch127_Booking?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

//Prompts a message in the terminal once the connection is 'open' and we are able to succesfully connect to our database
mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas'));

//allows all resources/origins to access our backend application
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

//"/users" to be included for all user routes defined in the "userRoutes" file
//"http://localhost:4000/users"
app.use('/users', userRoutes); //based routes for user routes
app.use('/courses', courseRoutes); //based routes for course routes





//will used the defined port number for the application whenever an environment variable is available OR will used port 4000 if non is defined
//This syntax will allow flexibility when using application locally or as a hosted application
app.listen(process.env.PORT || 4000, ()=> {
    console.log(`API is now online on port ${ process.env.PORT || 4000 }`)
})

/**
 * Activity:
1. Create a /details route that will accept the user’s Id to retrieve the details of a user.
2. Create a getProfile controller method for retrieving the details of the user:
	a. Find the document in the database using the user's ID
	b. Reassign the password of the returned document to an empty string
	c. Return the result back to the frontend
3. Process a GET request at the /details route using postman to retrieve the details of the user.
4. Create a git repository named S33.
5. Add another remote link and push to git with the commit message of Add s33 Activity.
6. Add the link in Boodle named Express js - API Development Part 2
 */