const express = require('express');
const router = express.Router();
const courseController = require('../controllers/courseControllers');
const auth = require('../auth');


//Route for creating a course
//nakita na is admin sya and authorize sya
router.post('/', auth.verify, (req, res) => {
    const data = {
        course: req.body, //dito galing ung nasa controllers
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    courseController.addCourse(data).then(result => res.send(result));
})
 
//retrieve all course
router.get('/all', (req,res) => {
    courseController.getAllCourses().then(result => res.send(result));
})

//retrieve all true courses
router.get('/', (req, res) => {
    courseController.getAllActive().then(result => res.send(result));

})

//retrieve specifi course
// router.get("/:courseId", (req,res)=>{
// 	courseController.specificCourse(req.params)
// 	.then(result =>{
// 		res.send(result)
// 	})
// })


router.get("/:id", (req,res)=>{
	courseController.specificCourse(req.params.id)
	.then(result =>{
		res.send(result)
	})
})

//Activity: SIr PAulo code: https://gitlab.com/fernandezpaulo0724-zuitt-projects/b127/s35-a
//update a course
router.put('/:courseId', auth.verify, (req, res) => {

    const data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    
    }
    if(data.isAdmin){
        courseController.updateCourse(req.params, req.body).then(result => res.send(result));
    }else{
        res.send(false);
    }

})


//archive a course/soft delete a course
router.put('/:courseId/archive', auth.verify, (req,res) => {
    // courseController.archiveCourse(req.params).then(result => res.send(result))

    const data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    
    }

    if(data.isAdmin){
        courseController.archiveCourse(req.params).then(result => res.send(result));
    }else{
        res.send(false);
    }
})



module.exports = router;